

# 🤖 Habitica discord bot
Unofficial Discord bot for showing the current stats of any profile

## 📦 Installation

```bash
npm install
```

## ⚙️ Usage

```bash
npm start
```

## 📩 Contributing
Pull requests are welcome.
For major changes, please open an issue first to discuss what you would like to change.

## 📝 License
This Repo is under the license [MIT](https://github.com/AdamSherif/habitica-discord-bot/blob/master/LICENSE)


## TODO
- add webhook support
- create stats command
- show profile picture in the profile command